# Chapter 3
- _Think in FP_
  - you see an application as
    - data flowing
    - data being transformed

# Chapter 4
- [Intro to Functional Game Programming with Scala](https://github.com/jdegoes/lambdaconf-2014-introgame)

# Chapter 6
- What is FP?
  - FP applications consist only of immutable values and pure functions
  - Pure Function ->
    a. output depends only on input
    a. functions have no side effects
- What is imperative programming?
  - statements change a program's state
  - describe _how_ a program operates
- Using FP means
  - we write what we _want_, not _how_ to do it

# Chapter 9
- Michael Feathers
  - OOP encapsulates moving parts
  - FP minimizes moving parts
- the best FP code is like algebra
  - variables are never reassigned

# Chapter 10
- lambda == anonymous function
- calculus == a formal system
- lisp pioneered
  - tree data structures
  - automatic storage management
  - dynamic typing
  - conditionals
  - higher-order functions
  - recursion
  - self-hosting compiler
- 
# Chapter 13
- math terms
  - `functor` = "Things that can be mapped over"
- FP terms
  - `lambda` = anonymous function
  - `combinator` = "a style of organizing libraries centered around combining things"
  - `higher-order function` = function that takes other functions as parameters

# Chapter 14
- pure function = output depends _only_ on input and no side effects
- impure functions do one or more of these things:
  - read hidden inputs
  - write hidden outputs
  - mutate parameters they are given
  - perform I/O with outside world
- impure function signatures
  - no input parameters
  - return nothing (or `Unit` in Scala)

# Chapter 15
- hidden input in an impure function can be called a "free variable"
- the state of the hidden input affects the output of the function

# Chapter 16
- Benefits of pure functions
  - easier to reason about
  - easier to combine (functional composition)
    - `val x = doThis(a).thenThis(b),andThenThis(c)`
  - easier to test
  - easier to debug
  - easier to parallelize
  - are idempotent
    - can be run many times with result the same as running only once for the same input
  - are referentially transparent
    - can be replaced with the resulting value without changing the program
  - are memoizable
    - cache the result of an "expensive" function call
  - can be lazy
    - only executed when the value is required, otherwise it is not executed
- 
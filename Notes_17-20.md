# Chapter 17
- how to do anything with FP?
  - write as much as possible with pure functions
  - handle UI and all I/O (database, web, file) with impure functions
  - wrap I/O functions in the `IO` type e.g. `def f: IO[String] = ???`

# Chapter 18
- function signatures are way more important in FP than OOP

# Chapter 19
- algebra lets you consider problems abstractly
- algebra is predictable
- use `copy` from a `case class` in Scala
  - `val p2 = p.copy(name = "New Name")`
- algebra in types are "sums" or "products"

# Chapter 20
- expression-oriented programming
  - every line evaluates to a result, therefore is an _expression_
  - statements (OOP)
    - `order.calculateTaxes()`
    - `order.updatePrices()`
  - expressions (FP)
    - `val tax = calculateTax(order)`
    - `val price = calculatePrice(order)`
    - if/then
    - match
    - try/catch



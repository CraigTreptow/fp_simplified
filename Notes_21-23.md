# Chapter 21
- Unix pipelines are FP
- shell pipelines are FP combinators
  - e.g. `who.cut(delimeter=" ", field=1).uniq.wc(lines = true).tr(find=" ", replace=" ")`

# Chapter 22
- function assigned to variable with a single input param
  - `val isEven = (i: Int) => i % 2 == 0`
  - transforms `Int` into `Boolean`

# Chapter 23
- methods and functions are equivalent when used as a parameter
- this is due to scala doing eta expansion for use
  - the compiler turns a method into a function
  - `val someF = someFMethod _`
- `val double = (i: Int) => i * 2    // function`
- `scala> def triple(i: Int) = i * 3 // method`
- most people seem to prefer the `def` so, since that creates a method
- this shows that it can be used like a function and also
- turned into a partially applied function by giving it a `_`

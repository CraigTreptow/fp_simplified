# Chapter 24
- `predicate` => a function that returns a `Boolean` value
- defining a function that takes a function as input parameter
  - define the signature of the function you want to accept
    - no input and return nothing: `callback: () => Unit`
    - two `Int` params and return `Int`: `callback: (Int, Int) => Int`
    - `variableName: (parameterTypes ...) => returnType`
- take function param with other params
- `blah(f: () => Unit, n: Int)`

# Chapter 25
# Chapter 26
- "call by-value" in Scala
  - either a primitive value (`Int`)
  - or a pointer to something like a `Person`
  - receiving a `val` where it is evaluated when the param is bound to the function
- "call by-name" in Scala
  - receiving a `def`where it is evaluated whenever it is used
  - `def timer(blockOfCode: => theReturnType`
  - `def timer[A](blockOfCode: => A) = ???`

# Chapter 27
 - multiple parameter groups
   - `def foo(a: Int, b: String)(c: Double)`
   - must call like `foo(1, "2")(1.2)`
 - allow both implicit and non-implicit parameters
 - facilitate type inference
 - param in one group can use param from previous group as default value
 - allow you to create your own control structure
 - use with `implicit` in the last group
   - e.g. `Future`
 - 
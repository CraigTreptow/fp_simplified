# Chapter 28
- partially-applied functions are useful
- multiple parameter groups makes it a little easier to create PAFs
- currying
  - a function that takes multiple parameters can be translated
  - into a series of functions that each take a single parameter
- `def plus2 = plus(2)(_)`
- `val plus2 = plus(2)(_: Int)`
  - specifying the type might be necessary and is probably nicer
- create a "curried" function from a "regular" function
  - `def add(x: Int, y: Int) = x + y`
  - `val addFunction = add _`
  - `val addCurried = (add _).curried`
  - `addCurried(1)(2)`
  - `val addCurriedTwo = addCurried(2) // create PAF`
  - `addCurriedTwo(10)                 // use PAF`
- currying is *always* done in Haskell
- PAF seems more useful for Scala

# Chapter 29
# Chapter 30
- `for` loops require `var`, FP doesn't use `var`, hence we need recursion
- loop code boilerplate is a waste of RAM in our brains
# Chapter 31
- `List` is a linked list
  - each cell contains a value and a pointer to the next cell
  - `Nil` is the value of the last cell
- `List(1,2,3)` <===> `1 : 2 :: 3 :: Nil`
# Chapter 32
- equivalent
  - `case List() => 0` and `case Nil => 0`
- e.g. `case head :: tail => head + sum(tail)`

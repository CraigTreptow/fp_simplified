# Chapter 33 & 34
- ways to visualize recursion

# Chapter 35
- more telling somebody how recursion works

# Chapter 36
- Three steps
  1. What is the function signature?
  2. What is the end condition for this algorithm?
  3. What is the actual algorithm?

# Chapter 37
- default stack size is now usually 1MB
- stack is the "call stack"
  - each JVM thread has a stack
  - the stack stores frames (stack frames)
  - stack frames hold partial results, local variables, etc
  - stack frames hold the thread's state
  - the JVM only pushes or pops stack frames to/from the stack
- stack frame
  - three parts: local variables, operand stack, frame data
  - created every time a method is called

# Chapter 38
- 

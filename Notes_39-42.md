# Chapter 39
- tail recursion solves stack overflow errors
- only a single stack frame is required
  - compiler can jump back to beginning of function with new params
- pattern to be tail recursive
  - for `sum`, add a new function with an accumulator
  - this function is called and does the recursion
  - the original function calls the new `private` function
  - the new function is often put _inside_ of the original function
- use `@tailrec` annotation
  - at least in Scala2, not sure about Scala3

# Chapter 40
- Almost every app needs a _state_ saved to function

# Chapter 41
- coin flip example app review

# Chapter 42
- `case class` review
  - gives you `apply` method (no `.new` is required)
  - accessor methods created for `val` fields
  - mutator methods created as well for `var` fields (don't use these)
  - `unapply` is generated for you
  - `copy` is generated for you
  - `equals` and `hashCode` are generated for you
  - `toString` is generated for you
- 

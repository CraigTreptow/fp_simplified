# Chapter 43
- update as you copy, rather than mutating
- libraries known as "lenses" make copying nested objects simpler
- examples
  - `scalaz.Lens`
  - `Quicklens`
  - `Sauron`
  - `Monacle`

# Chapter 44
- list comprehension
  - used to filter, transform, and combine lists
  - `for (enumerators) yield e`
    - enumerators are either generators or filters
  - in Scala they can contain generators, filters, or definitions
  - `p <- persons`, `if (n startsWith "To")`, `n = p.name`
  - always begins with a generator
  - can have multiple generators
  - left side of a generator can be a pattern

# Chapter 45
- write custom class for using inside for expression
  - must implement
    - `map`
    - `flatMap`
    - `foreach`
    - `withFilter`


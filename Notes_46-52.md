# Chapter 46
- `initialElems: A*)`
- above uses `varargs`
- the `*` lets you pass the constructor varying numbers of elements
  - `Sequence(1,2)`, `Sequence('a','b','c','d')`

# Chapter 47
# Chapter 48
- `abMap: _*` adapt a collection to work with `varargs`
# Chapter 49
- preferred to implement `withFilter` instead of `filter` now (as of 2.8)
# Chapter 50
# Chapter 51 & 52
- pure functions *never* through exceptions
- rather, handle the errors and return an `Option`

# Chapter 53
- `Option`, `Some`, `None`
  - doesn't say anything about _why_ the failure happened
- `Try`, `Success`, `Failure`
  - `Failure` contains the reason it failed
  - also has `getOrElse`
- `Either`, `Left`, `Right`
  - prior to scala 2.10
  - _convention only_
    - `Left` holds the error
    - `Right` holds the value
  - doesn't work well with `for` expressions
- `Or` from the `Scalactic` library
  - 3rd party, must add in _build.sbt_ file
  - Al uses `Option` or `Or` these days
  - favors `Good` over `Bad`
  - is more convenient if that is what you want
- also consider the Null Object pattern
  - perhaps return an empty list in the error case
  - so the result has the same shape either way
- use these same techniques when dealing with `null` values

# Chapter 54
- embrace the idioms
- write Scala code, not some other language using Scala

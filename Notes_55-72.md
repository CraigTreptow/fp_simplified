# Chapter 55
# Chaspter 56
- the `for` expression is syntatic sugar for `flatMap`
# Chapter 57, 58, 59
-`getOrElse` can lead to incorrect results
  - like with `makeInt`
  - if one fails, the others work and the numbers are added
  - however, the result *does not* contain the value that 
  - could not be converted, which is an incorrect answer
# Chapter 60
- any class that implements `map` and `flatMap` can be used in for expressions
- scala collection classes have `map` and `flatMap`
- `Future` has both functions as well

# Chapter 61
- `functor` - things that can be mapped over
- `List`, `Option`, `Future`, etc.
- a class with a `map` method is a `functor`
- 
# Chapter 62, 63, 64, 65, 66, 67, 68
- all steps in these chapters lead to writing a `monad`

# Chapter 69, 70, 71, 72
- any function that _returns_ a type that _implements_ `map`, and `flatMap` works in a `for`
- `Debuggable` example is an implementation of Haskell's `Writer` monad
- allows computations, while combining all the "log values" into one at the end



# Chapter 79, 80, 81, 82
- `IO` let's you write I/O functions as _effects_
- use `IO` to _describe_ how an I/O function works
- you defer _action_ with I/O until later, when you really need it
  - not at the time it _appears_ to be called
- `IO` lets you write code with side effects in a compositional style

# Chapter 81
- _non-composed_ code uses temp variables to hold intermediate state
  - `val y = g(x); val z = f(y)`
- _composed_ code makes the compiler do that
  - `val z = f(g(x))`

# Chapter 82, 83, 84, 85
- the `State` monad makes "state" easier to work with in for expressions
- John De Goes has a talk
  - Introduction to Funcational Game Programming with Scala
-
